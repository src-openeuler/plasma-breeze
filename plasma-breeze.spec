%undefine __cmake_in_source_build

## bootstrap, omit problematic optional build deps)
#global bootstrap 1

%global         base_name   breeze

Name:           plasma-breeze
Version:        5.27.12
Release:        1
Summary:        Artwork, styles and assets for the Breeze visual style for the Plasma Desktop

License:        GPLv2+
URL:            https://cgit.kde.org/%{base_name}.git

%global majmin %majmin_ver_kf5
%global stable %stable_kf5
Source0:        http://download.kde.org/%{stable}/plasma/%{version}/%{base_name}-%{version}.tar.xz

# filter plugin provides
%global __provides_exclude_from ^(%{_kf5_qtplugindir}/.*\\.so)$

BuildRequires:  gettext
BuildRequires:  kdecoration-devel >= %{majmin_ver}

BuildRequires:  extra-cmake-modules
BuildRequires:  kf5-kauth-devel
BuildRequires:  kf5-frameworkintegration-devel
BuildRequires:  kf5-kcmutils-devel
BuildRequires:  kf5-kcompletion-devel
BuildRequires:  kf5-kconfig-devel
BuildRequires:  kf5-kcoreaddons-devel
BuildRequires:  kf5-kguiaddons-devel
BuildRequires:  kf5-ki18n-devel
BuildRequires:  kf5-kirigami2-devel
BuildRequires:  kf5-kpackage-devel
BuildRequires:  kf5-kservice-devel
BuildRequires:  kf5-kwayland-devel
BuildRequires:  kf5-kwindowsystem-devel
BuildRequires:  kf5-plasma-devel
BuildRequires:  kf5-rpm-macros

BuildRequires:  libxcb-devel
BuildRequires:  fftw-devel
%if 0
# required kpackage plugins
BuildRequires:  plasma-packagestructure
%endif

BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qtdeclarative-devel
BuildRequires:  qt5-qtx11extras-devel

Requires:       %{name}-common = %{version}-%{release}

# since we provide a cmake dev-like file
Provides:       %{name}-devel = %{version}-%{release}

%description
%{summary}.

%package        common
Summary:        Common files shared between KDE 4 and Plasma 5 versions of the Breeze style
BuildArch:      noarch
%description    common
%{summary}.

%package -n     breeze-cursor-theme
Summary:        Breeze cursor theme
BuildArch:      noarch
Obsoletes:      breeze-icon-theme < 5.17.0
Provides:       breeze-cursor-themes = %{version}-%{release}
%description -n breeze-cursor-theme
%{summary}.


%prep
%autosetup -n %{base_name}-%{version} -p1


%build
%{cmake_kf5}

%cmake_build


%install
%cmake_install

%find_lang breeze --all-name


%ldconfig_scriptlets

%files
%license LICENSES/*.txt
%{_kf5_qtplugindir}/org.kde.kdecoration2/breezedecoration.so
%{_kf5_qtplugindir}/styles/breeze.so
%{_kf5_qtplugindir}/plasma/kcms/breeze/kcm_breezedecoration.so
%{_kf5_qtplugindir}/plasma/kcms/systemsettings_qwidgets/breezestyleconfig.so
%{_kf5_datadir}/kstyle/themes/breeze.themerc
%{_kf5_datadir}/kconf_update/breezehighcontrasttobreezedark.upd
%{_kf5_datadir}/kconf_update/breezetobreezeclassic.upd
%{_kf5_datadir}/kconf_update/breezetobreezelight.upd
%{_kf5_datadir}/applications/breezestyleconfig.desktop
%{_kf5_datadir}/applications/kcm_breezedecoration.desktop
%{_kf5_libdir}/kconf_update_bin/breezehighcontrasttobreezedark
%{_kf5_libdir}/kconf_update_bin/breezetobreezeclassic
%{_kf5_libdir}/kconf_update_bin/breezetobreezelight
# used by breezedecoration
%{_libdir}/libbreezecommon5.so.5*
%{_bindir}/breeze-settings5
%{_datadir}/icons/hicolor/*/apps/breeze-settings.*
%{_libdir}/cmake/Breeze/

%files common -f breeze.lang
%{_datadir}/color-schemes/*.colors
%dir %{_datadir}/QtCurve/
%{_datadir}/QtCurve/Breeze.qtcurve
%{_datadir}/wallpapers/Next/

%files -n breeze-cursor-theme
%doc cursors/Breeze/README
%dir %{_kf5_datadir}/icons/Breeze_Snow/
%{_kf5_datadir}/icons/Breeze_Snow/cursors/
%{_kf5_datadir}/icons/Breeze_Snow/index.theme
%dir %{_kf5_datadir}/icons/breeze_cursors/
%{_kf5_datadir}/icons/breeze_cursors/cursors/
%{_kf5_datadir}/icons/breeze_cursors/index.theme


%changelog
* Mon Feb 17 2025 lvfei <lvfei@kylinos.cn> - 5.27.12-1 
- Update package to version 5.27.12

* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 5.27.11-2
- adapt to the new CMake macros to fix build failure

* Thu Mar 14 2024 peijiankang <peijiankang@kylinos.cn> - 5.27.11-1
- Update package to version 5.27.11

* Mon Jan 08 2024 zhangxianting <zhangxianting@unintech.com> - 5.27.10-1
- update to upstream version 5.27.10

* Fri Aug 04 2023 yajun<yajun@kylinos.cn> - 5.27.6-1
- update to upstream version 5.27.6

* Tue Dec 13 2022 lijian <lijian2@kylinos.cn> - 5.26.4-1
- update to upstream version 5.26.4

* Mon Sep 05 2022 liweiganga <liweiganga@uniontech.com> - 5.25.4-1
- update to upstream version 5.25.4

* Tue Jul 05 2022 peijiankang <peijiankang@kylinos.cn> - 5.25.2-1
- update to upstream version 5.25.2

* Mon Feb 14 2022 peijiankang <peijiankang@kylinos.cn> - 5.24.0-1
- update to upstream version 5.24.0

* Tue Jan 18 2022 pei-jiankang <peijiankang@kylinos.cn> - 5.18.5-1
- update to upstream version 5.18.5

* Fri Aug 07 2020 weidong <weidong@uniontech.com> - 5.16.5-1
- Initial release for OpenEuler
